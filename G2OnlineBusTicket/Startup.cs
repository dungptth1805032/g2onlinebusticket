﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(G2OnlineBusTicket.Startup))]
namespace G2OnlineBusTicket
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
