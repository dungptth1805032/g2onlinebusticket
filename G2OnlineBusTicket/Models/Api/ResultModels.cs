﻿using System;

namespace G2OnlineBusTicket.Models.Api
{
    public class StationResult
    {
        public int id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string lat { get; set; }
        public string lng { get; set; }
    }

    public class SeatResult
    {
        public int id { get; set; }
        public int busId { get; set; }
        public string name { get; set; }
        public Nullable<System.DateTime> expiredAt { get; set; }
        public bool isBooked { get; set; }
    }

    public class JourneyDetailResult : Journey {
        public JourneyDetailResult(Journey journey)
        {
            id = journey.id;
            bus_id = journey.bus_id;
            type = journey.type;
            start_time = journey.start_time;
            arrival_time = journey.arrival_time;
            created_at = journey.created_at;
            modified_at = journey.modified_at;
            BookingDetails = journey.BookingDetails;
            Bus = journey.Bus;
            Routes = journey.Routes;
        }
        public string startLocation { get; set; }
        public string endLocation { get; set; }
        public DateTime departure { get; set; }
        public DateTime arrival { get; set; }
    }
}