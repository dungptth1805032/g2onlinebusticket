﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2OnlineBusTicket.Models.Api
{
    public class TicketForm
    {
        public int id { get; set; }
        public string name { get; set; }
        public int journeyID { get; set; }
        public string startLocation { get; set; }
        public string endLocation { get; set; }
        public string departure { get; set; }
        public string arrival { get; set; }
        public string licensePlate { get; set; }
        public int distance { get; set; }
        public string busType { get; set; }
        public int price { get; set; }
        public int caculatePrice { get; set; }
        public DateTime expireDate { get; set; }
    }
}