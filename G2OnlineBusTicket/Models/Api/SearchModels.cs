﻿using System;
using System.ComponentModel.DataAnnotations;

namespace G2OnlineBusTicket.Models.Api
{
    public class JourneySearch
    {
        [Display(Name = "From")]
        public string startLocation { get; set; }

        [Display(Name = "To")]
        public string endLocation { get; set; }

        [Display(Name = "Bus type")]
        public string busType { get; set; }

        [Display(Name = "Departure")]
        public DateTime startDate { get; set; }
    }

    public class BookingDetailSearch
    {
        [Display(Name = "Booking ID")]
        public int bookingDetailId { get; set; }

        [Display(Name = "Citizenship ID")]
        public string citizenshipId { get; set; }
    }

    public class TicketSearch
    {
        [Display(Name = "Ticket ID")]
        public int ticketId { get; set; }

        [Display(Name = "Citizenship ID")]
        public string citizenshipId { get; set; }
    }
}