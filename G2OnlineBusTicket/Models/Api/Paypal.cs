﻿using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace G2OnlineBusTicket.Models.Api.Logic
{
    public class PaypalConfiguration
    {
        public readonly static string clientId;
        public readonly static string ClientSecret; 

        static PaypalConfiguration()
        {
            var config = GetConfig();
            clientId = config["clientId"];
            ClientSecret = config["clientSecret"];
        }

        public static Dictionary<string, string> GetConfig()
        {
            return PayPal.Api.ConfigManager.Instance.GetProperties();
        }

        private static string GetAccessToken()
        {
            string accessToken = new OAuthTokenCredential(clientId, ClientSecret, GetConfig()).GetAccessToken();
            return accessToken;
        }

        public static APIContext GetAPIContext()
        {
            APIContext apiContext = new APIContext(GetAccessToken());
            apiContext.Config = GetConfig();
            return apiContext;
        }
    }

    public class PaypalPaymentAction
    {
        public static Payment CreatePayment(APIContext apiContext, string redirectUrl, List<Ticket> tickets)
        {
            decimal subtotal = 0;
            decimal total = 0;
            int invoiceNumber = ClientLogicModels.db.Tickets.Find(tickets.FirstOrDefault().id).BookingDetail.id;
            var ticketList = new ItemList()
            {
                items = new List<Item>()
            };

            foreach(var ticket in tickets)
            {
                var usdPrice = (ticket.price / 25000);
                ticketList.items.Add(new Item()
                {
                    name = "Seat",
                    currency = "USD",
                    price = usdPrice.ToString(),
                    quantity = "1",
                    sku = "sku"
                });
                subtotal += usdPrice;
            }

            var payer = new Payer()
            {
                payment_method = "paypal"
            };
 
            var redirUrls = new RedirectUrls()
            {
                cancel_url = redirectUrl + "&Cancel=true",
                return_url = redirectUrl
            };

            var details = new Details()
            {
                tax = "1",
                shipping = "0",
                subtotal = subtotal.ToString()
            };

            total = Convert.ToInt32(details.tax) + Convert.ToInt32(details.shipping) + Convert.ToInt32(details.subtotal);

            var amount = new Amount()
            {
                currency = "USD",
                total = total.ToString(), 
                details = details
            };

            var transactionList = new List<Transaction>();
            transactionList.Add(new Transaction()
            {
                description = "Tickets checkout",
                invoice_number = invoiceNumber.ToString(),
                amount = amount,
                item_list = ticketList
            });

            var payment = new Payment()
            {
                intent = "sale",
                payer = payer,
                transactions = transactionList,
                redirect_urls = redirUrls
            };

            return payment.Create(apiContext);
        }

        public static Payment ExecutePayment(APIContext apiContext, string payerId, string paymentId)
        {
            var paymentExecution = new PaymentExecution()
            {
                payer_id = payerId
            };

            var payment = new Payment()
            {
                id = paymentId
            };

            return payment.Execute(apiContext, paymentExecution);
        }
    }
}