﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace G2OnlineBusTicket.Models.Api.Logic
{
    public class ClientLogicModels
    {
        public static G2OnlineBusTicketEntities db = new G2OnlineBusTicketEntities();

        // Support
        public static bool IsSeatBooked(int seatId, int journeyId, DateTime startDate)
        {
            var startDateLimit = startDate.AddDays(1);
            var seat = db.Seats.Find(seatId);
            var ticket = db.Tickets
                .Where(t => t.seat_id == seatId)
                .Where(t => t.BookingDetail.journey_id == journeyId)
                .Where(t => t.BookingDetail.start_time >= startDate && t.BookingDetail.start_time <= startDateLimit)
                .ToList();
            if (seat.expired_at > DateTime.Now || ticket.Count() > 0)
            {
                return true;
            }
            return false;
        }

        public static ICollection<Journey> SearchForJourneys(JourneySearch searchQuery)
        {
            var result = new List<Journey>();
            try
            {
                var startDate = searchQuery.startDate;
                var startDateLimit = searchQuery.startDate.AddDays(1);
                result = db.Journeys
                    .Where(j => (j.start_time >= startDate && j.start_time <= startDateLimit) || (j.type == "Daily" && j.start_time <= startDate && j.arrival_time >= startDate))
                    .Where(j => j.Bus.type == searchQuery.busType)
                    .Where(j => j.Routes.Where(r => r.Station.name == searchQuery.startLocation).Count() > 0)
                    .Where(j => j.Routes.Where(r => r.Station.name == searchQuery.endLocation).Count() > 0)
                    .Where(j => j.Routes.Where(r => r.Station.name == searchQuery.startLocation).FirstOrDefault().id < j.Routes.Where(r => r.Station.name == searchQuery.endLocation).FirstOrDefault().id)
                    .ToList();
            }
            catch { }
            return result;
        }

        public static List<List<SeatResult>> GetSeatGrid(int journeyId, DateTime startDate)
        {
            var result = new List<List<SeatResult>>();
            var temporaryList = new List<SeatResult>();
            var selectedJourney = db.Journeys.Find(journeyId);
            foreach (var seat in selectedJourney.Bus.Seats)
            {
                var resultItem = new SeatResult
                {
                    id = seat.id,
                    name = seat.name,
                    busId = seat.bus_id,
                    expiredAt = seat.expired_at,
                    isBooked = IsSeatBooked(seat.id, journeyId, startDate)
                };
                if (temporaryList.Count() < 4)
                {
                    temporaryList.Add(resultItem);
                }
                else
                {
                    result.Add(new List<SeatResult>(temporaryList));
                    temporaryList.Clear();
                    temporaryList.Add(resultItem);
                }
            }
            return result;
        }

        public static List<JourneyDetailResult> GetJourneyDetail(ICollection<Journey> journeys, string startLocation, string endLocation, DateTime startDate)
        {
            var result = new List<JourneyDetailResult>();
            foreach (var journey in journeys)
            {
                var resultItem = new JourneyDetailResult(journey);
                var timeFromStart = new TimeSpan(0);
                var time = new TimeSpan(0);
                var isDone = false;
                var incremental = false;
                foreach (var route in journey.Routes)
                {
                    if (!isDone)
                    {
                        timeFromStart = !incremental ? timeFromStart.Add(route.time_from_previous_station) : timeFromStart;
                        time = incremental ? time.Add(route.time_from_previous_station) : time;
                        if (route.Station.name == startLocation)
                        {
                            incremental = true;
                        }
                        if (route.Station.name == endLocation)
                        {
                            incremental = false;
                            isDone = true;
                        }
                    }
                }
                resultItem.startLocation = startLocation;
                resultItem.endLocation = endLocation;
                resultItem.departure = startDate.Date.Add(resultItem.start_time.TimeOfDay.Add(timeFromStart));
                resultItem.arrival = resultItem.departure.Add(time);
                result.Add(resultItem);
            }
            return result;
        }

        public static List<Ticket> SaveBookingInformation(FormCollection form)
        {
            var result = new List<Ticket>();
            try
            {
                var bookerID = form["inputBookerIDIn"];
                var customers = db.Customers.Where(customer => customer.citizenship_id == bookerID).ToList();
                var customerID = customers.Count() > 0 ? customers.FirstOrDefault().id : -1;

                if (customerID < 0)
                {
                    // Add new customer
                    var newCustomer = new Customer();
                    newCustomer.name = form["inputBookerNameIn"];
                    newCustomer.address = form["inputBookerAddressIn"];
                    newCustomer.citizenship_id = form["inputBookerIDIn"];
                    newCustomer.email = form["inputBookerEmailIn"];
                    newCustomer.phone = form["inputBookerPhoneIn"];
                    db.Customers.Add(newCustomer);
                    customerID = newCustomer.id;
                }

                var ticketListStr = form["hiddenPrice"].TrimEnd(',');
                var ticketListJson = JsonConvert.DeserializeObject<List<TicketForm>>(ticketListStr);
                var totalPrice = 0;

                foreach (var ticket in ticketListJson)
                {
                    totalPrice += ticket.caculatePrice;
                }

                var newPaymentDetail = new PaymentDetail();
                newPaymentDetail.type = form["paymentOption"];
                newPaymentDetail.amount = totalPrice;
                newPaymentDetail.status = "pending";
                newPaymentDetail.canceled = false;
                newPaymentDetail.refund = 0;
                db.PaymentDetails.Add(newPaymentDetail);

                var startTime = ticketListJson.FirstOrDefault().departure;
                var arrivalTime = ticketListJson.FirstOrDefault().arrival;

                var newBookingDetail = new BookingDetail();
                newBookingDetail.customer_id = customerID;
                newBookingDetail.journey_id = ticketListJson.FirstOrDefault().journeyID;
                newBookingDetail.from_location = ticketListJson.FirstOrDefault().startLocation;
                newBookingDetail.to_location = ticketListJson.FirstOrDefault().endLocation;
                newBookingDetail.start_time = Convert.ToDateTime(startTime);
                newBookingDetail.arrival_time = Convert.ToDateTime(arrivalTime);
                newBookingDetail.payment_detail_id = newPaymentDetail.id;
                newBookingDetail.status = true;
                db.BookingDetails.Add(newBookingDetail);

                foreach (var ticket in ticketListJson)
                {
                    var newTicket = new Ticket();
                    newTicket.booking_detail_id = newBookingDetail.id;
                    newTicket.customer_name = form["inputNameIn" + ticket.id + ""];
                    newTicket.citizenship_id = form["inputIDIn" + ticket.id + ""];
                    newTicket.age = Convert.ToInt32(form["inputAgeIn" + ticket.id + ""]);
                    newTicket.price = ticket.caculatePrice;
                    newTicket.seat_id = ticket.id;
                    db.Tickets.Add(newTicket);
                    result.Add(newTicket);
                }

                db.SaveChanges();
            }
            catch { }
            return result;
        }
    }
}