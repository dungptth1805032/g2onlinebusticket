﻿$(function () {
    $('.date-picker').datepicker({
        minDate: new Date()
    });
});

function initMap() {
    var map = new google.maps.Map(document.getElementById('homeIndex__map'), {
        zoom: 5,
        center: new google.maps.LatLng(17.047849, 106.929226),
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();
    var marker, i;

    fetch("/api/OBTApi/GetStations")
        .then(res => res.json())
        .then((results) => {
            for (i = 0; i < results.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(results[i].lat, results[i].lng),
                    map: map
                });
                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        infowindow.setContent(results[i].name);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        });
}

updateCart();