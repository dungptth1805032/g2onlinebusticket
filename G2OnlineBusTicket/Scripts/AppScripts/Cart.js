﻿window.expiredTicketRemove = () => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats = seats.filter((seat) => {
        const time = (new Date(seat.expireDate).getTime()) - (new Date().getTime());
        return time > 0 ? true : false;
    });
    localStorage.setItem("seats", JSON.stringify(seats));
}

window.setCountdown = (elementID, expireDate) => {
    const deadline = new Date(expireDate).getTime();
    const timer = setInterval(function () {
        const time = deadline - (new Date().getTime());
        if (time > 0) {
            const minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((time % (1000 * 60)) / 1000);
            const newElement = `${minutes}m ${seconds}s before expire`;
            try {
                document.getElementById(elementID).innerHTML = newElement;
            } catch {
                clearInterval(timer);
            }
        } else {
            clearInterval(timer);
            document.getElementById(elementID).innerHTML = "Expire";
            cancelThisSeat(elementID.substring(6));
        }
    }, 1000);
};

window.addNewElementToCart = (newElement) => {
    $("#OBT__cart").append(newElement);
}

window.updateCart = () => {
    expiredTicketRemove();
    $("#OBT__cart").empty();
    const seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];

    if (seats.length < 1) {
        addNewElementToCart(`<p class="text-danger">Your cart is empty</p>`);
        return;
    }

    seats.forEach((seat) => {
        const newElement = `
            <div class="OBT__cartItem">
                <strong>Seat: </strong>${seat.name}
                <br/>
                <strong>From </strong>${seat.startLocation}<strong> to </strong>${seat.endLocation}
                <br/>
                <strong>Start: </strong>${seat.departure}
                <br/>
                <strong>Arrival: </strong>${seat.arrival}
                <br/>
                <strong>Bus: </strong>${seat.licensePlate}
                <br/>
                <strong id="timer_${seat.id}" class="text-danger"></strong>
                <br/>
                <button class="btn-danger" onclick="cancelThisSeat(${seat.id})">Cancel</button>
            </div>`;
        addNewElementToCart(newElement);
        setCountdown(`timer_${seat.id}`, seat.expireDate);
    });
}

window.selectThisSeat = (id, name, journeyID, startLocation, endLocation, departure, arrival, licensePlate) => {
    const urlParams = new URLSearchParams(window.location.search);
    const startDate = urlParams.get('startDate');
    const busType = urlParams.get('busType');
    const getSeatStatusURL = `/api/OBTApi/GetSeatStatus/${id}?journeyID=${journeyID}&startDate=${startDate}`;
    const getDistanceURL = `/api/OBTApi/GetDistance/1?journeyID=${journeyID}&startLocation=${startLocation}&endLocation=${endLocation}`;

    fetch(getSeatStatusURL)
        .then((seatStatus) => seatStatus.json())
        .then((seatStatus) => {
            if (seatStatus) {
                $(`#seat_${id}`).replaceWith(`<img id="seat_${id}" src="/Content/Images/booked_seat.gif"/>`);
                return;
            }
            fetch(getDistanceURL)
                .then((distance) => distance.json())
                .then((distance) => {
                    const getPriceURL = `/api/OBTApi/GetPrice/1?busType=${busType}&distance=${distance}`;
                    fetch(getPriceURL)
                    .then((price) => price.json())
                    .then((price) => {
                        let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
                        let expireDate = new Date((new Date).getTime() + 300000);
                        seats.push({
                            "id": id,
                            "name": name,
                            "journeyID": journeyID,
                            "startLocation": startLocation,
                            "endLocation": endLocation,
                            "departure": departure,
                            "arrival": arrival,
                            "licensePlate": licensePlate,
                            "distance": distance,
                            "busType": busType,
                            "price": price,
                            "caculatePrice": price,
                            "expireDate": expireDate
                        });
                        localStorage.setItem("seats", JSON.stringify(seats));
                        $(`#seat_${id}`).replaceWith(`<img id="seat_${id}" class="active" src="/Content/Images/selected_seat.gif" onclick="cancelThisSeat(${id})"/>`);
                        updateCart();
                    });
                });
        });
};

window.cancelThisSeat = (id) => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    let matchedSeat = seats.filter(s => s.id == id)[0];
    if (matchedSeat) {
        const getCancelSatusURL = `/api/OBTApi/GetCancelSatus/${id}?journeyID=${matchedSeat.journeyID}`;
        fetch(getCancelSatusURL)
            .then((cancelSatus) => cancelSatus.json())
            .then((cancelSatus) => {
                const newElement = `
                    <img id="seat_${id}"
                         class="active"
                         src="/Content/Images/available_seat.gif"
                         onclick="selectThisSeat(
                                    ${id},
                                    '${matchedSeat.name}',
                                    ${matchedSeat.journeyID},
                                    '${matchedSeat.startLocation}',
                                    '${matchedSeat.endLocation}',
                                    '${matchedSeat.departure}',
                                    '${matchedSeat.arrival}',
                                    '${matchedSeat.licensePlate}')"/>`;
                $(`#seat_${id}`).replaceWith(newElement);
                localStorage.setItem("seats", JSON.stringify(seats.filter((seat) => seat.id !== id)));
                updateCart();
            });
    }
}

document.getElementById("OBT__cartCheckout").addEventListener("click", function () {
    updateCart();
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    if (seats.length < 1) {
        alert("Your cart is empty");
    } else {
        window.location.href = "/Home/Checkout";
    }
});