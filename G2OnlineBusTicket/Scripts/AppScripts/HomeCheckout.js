﻿window.expiredTicketRemove = () => {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats = seats.filter((seat) => {
        const time = (new Date(seat.expireDate).getTime()) - (new Date().getTime());
        return time > 0 ? true : false;
    });
    localStorage.setItem("seats", JSON.stringify(seats));
}

window.setCountdown = (elementID, expireDate) => {
    const deadline = new Date(expireDate).getTime();
    const timer = setInterval(function () {
        const time = deadline - (new Date().getTime());
        if (time > 0) {
            const minutes = Math.floor((time % (1000 * 60 * 60)) / (1000 * 60));
            const seconds = Math.floor((time % (1000 * 60)) / 1000);
            const newElement = `${minutes}m ${seconds}s before expire`;
            try {
                document.getElementById(elementID).innerHTML = newElement;
            } catch {
                clearInterval(timer);
            }
        } else {
            clearInterval(timer);
            document.getElementById(elementID).innerHTML = "Expire";
            updateForm();
            loadPrice();
        }
    }, 1000);
};

function updateAge(elementID) {
    let output = document.getElementById(`inputAge${elementID}`).value;
    document.getElementById(`inputAgeText${elementID}`).innerHTML = output;
    updatePrice(elementID);
}

function updatePrice(elementID) {
    let output = document.getElementById(`inputAge${elementID}`).value;
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    let totalPrice = 0;

    seats.forEach((seat) => {
        if (seat.id == elementID && output < 10) {
            seat.caculatePrice = 0;
        } else if (seat.id == elementID && output > 10 && output <= 50) {
            seat.caculatePrice = seat.price;
        } else if (seat.id == elementID && output > 50) {
            seat.caculatePrice = seat.price * 0.7;
        }
        if (seat.id == elementID) {
            let newElement = seat.caculatePrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
            document.getElementById(`price${elementID}`).innerHTML = newElement;
        }
        totalPrice += seat.caculatePrice;
    });

    totalPrice = totalPrice.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    document.getElementById(`totalPrice`).innerHTML = totalPrice;
    localStorage.setItem("seats", JSON.stringify(seats));
    document.getElementById(`hiddenPrice`).value = JSON.stringify(seats);
}

function loadPrice() {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats.forEach((seat) => {
        updatePrice(seat.id);
    });
}

function cancelThisSeat(id) {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats = seats.filter(s => s.id != id);
    
    localStorage.setItem("seats", JSON.stringify(seats));
    loadPrice();
    $(`#ticket_${id}`).empty();

    if (seats.length < 1) {
        window.location.href = "/";
        return;
    }
}

function updateForm() {
    expiredTicketRemove();
    $("#ticketItems").empty();
    const seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    if (seats.length < 1) {
        window.location.href = "/";
        return;
    }
    seats.forEach((seat) => {
        const newElement = `
        <tr id="ticket_${seat.id}">
            <td>
                <div class="form-horizontal">
                    <div class="form-group">
                        <label for="inputName${seat.id}" class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input id="inputName${seat.id}" class="form-control" name="inputNameIn${seat.id}" type="text" placeholder="Full name" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputAge${seat.id}" class="col-sm-3 control-label">
                            Age (<span id="inputAgeText${seat.id}">18</span>)
                        </label>
                        <div class="col-sm-9">
                            <input id="inputAge${seat.id}" class="form-control" name="inputAgeIn${seat.id}" type="number" placeholder="Age" required value="18" min="1" max="150" onchange="updateAge(${seat.id})"/>
                        </div>     
                    </div>
                    <div class="form-group">
                        <label for="inputID${seat.id}" class="col-sm-3 control-label">ID</label>
                        <div class="col-sm-9">
                            <input id="inputID${seat.id}" class="form-control" name="inputIDIn${seat.id}" type="text" placeholder="Citizenship ID" required>
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <div class="OBT__cartItem">
                    <strong>Seat: </strong>${seat.name}<br/>
                    <strong>From </strong>${seat.startLocation}<strong> to </strong>${seat.endLocation}<br/>
                    <strong>Start: </strong>${seat.departure}<br/>
                    <strong>Arrival: </strong>${seat.arrival}<br/>
                    <strong>Bus: </strong>${seat.licensePlate}<br/>
                    <strong id="timer_${seat.id}" class="text-danger"></strong> <br/>
                    <button class="btn-danger" type="button" onclick="cancelThisSeat(${seat.id})">Cancel</button>
                </div>
            </td>
            <td>
                <div>
                    <strong>Bus Type: </strong><span id="busType${seat.id}"></span><br/>
                    <strong>Price: </strong><span id="busPrice${seat.id}"></span> (VND) per kilometer<br/>
                    <strong>Discount:</strong><br/>
                    <strong>- Infant (< 10 year old): </strong>Free<br>
                    <strong>- Adult: </strong>Full charge<br/>
                    <strong>- Elder (> 50 year old): </strong>30% discount
                    <hr/>
                    <strong>Your price: </strong><span id="price${seat.id}"></span> (VND)
                </div>
            </td>
        </tr>`;
        $("#ticketItems").append(newElement);
        setCountdown(`timer_${seat.id}`, seat.expireDate);
    });
}

updateForm();
loadPrice();