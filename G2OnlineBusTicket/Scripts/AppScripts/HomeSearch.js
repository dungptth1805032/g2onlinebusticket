﻿$(function () {
    $('.date-picker').datepicker({
        minDate: new Date()
    });
});

function loadSeat() {
    let seats = localStorage.getItem("seats") ? JSON.parse(localStorage.getItem("seats")) : [];
    seats.forEach((s) => {
        $(`#seat_${s.id}`).replaceWith(`<img id="seat_${s.id}" class="active" src="/Content/Images/selected_seat.gif" onclick="cancelThisSeat(${s.id})"/>`);
    });
};

function changeJourney(journeyId) {
    const urlParams = new URLSearchParams(window.location.search);
    const startLocation = urlParams.get("startLocation");
    const endLocation = urlParams.get("endLocation");
    const busType = urlParams.get("busType");
    const startDate = urlParams.get("startDate");
    if (confirm('You will lose all selected ticket. Are you sure you want to do it?')) {
        localStorage.setItem("seats", JSON.stringify([]));
        window.location.href = `/Home/Search/${journeyId}?startLocation=${startLocation}&endLocation=${endLocation}&busType=${busType}&startDate=${startDate}`;
    } else { }
}

updateCart();
loadSeat();