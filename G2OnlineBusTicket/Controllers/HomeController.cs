﻿using G2OnlineBusTicket.Models;
using G2OnlineBusTicket.Models.Api;
using G2OnlineBusTicket.Models.Api.Logic;
using PayPal.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace G2OnlineBusTicket.Controllers
{

    public class HomeController : Controller
    {
        public G2OnlineBusTicketEntities db = ClientLogicModels.db;
        public ActionResult Index()
        {
            var stations = new List<string>();
            var busTypes = new List<string>();
            try
            {
                stations = (from s in db.Stations select s.name).ToList();
                busTypes = (from b in db.Buses select b.type).Distinct().ToList();
            }
            catch { }
            ViewBag.stations = stations;
            ViewBag.busTypes = busTypes;
            return View();
        }

        [HttpPost]
        public ActionResult Index(JourneySearch searchQuery)
        {
            return RedirectToAction("Search", new
            {
                id = 1,
                searchQuery.startLocation,
                searchQuery.endLocation,
                searchQuery.busType,
                searchQuery.startDate,
            });
        }

        public ActionResult Search(int id, JourneySearch searchQuery)
        {
            ICollection<Journey> matchedJourneys = ClientLogicModels.SearchForJourneys(searchQuery);
            if (matchedJourneys.Count() > 0)
            {
                try
                {
                    var matchedJourneyDetails = ClientLogicModels.GetJourneyDetail(
                        matchedJourneys,
                        searchQuery.startLocation,
                        searchQuery.endLocation,
                        searchQuery.startDate);
                    var selectedJourney = id == 1
                       ? matchedJourneyDetails.FirstOrDefault()
                       : (matchedJourneyDetails.Where(j => j.id == id).FirstOrDefault() != null
                        ? matchedJourneyDetails.Where(j => j.id == id).FirstOrDefault()
                        : matchedJourneyDetails.FirstOrDefault());
                    ViewBag.matchedJourneys = matchedJourneyDetails;
                    ViewBag.selectedJourney = selectedJourney;
                    ViewBag.seatGrid = ClientLogicModels.GetSeatGrid(selectedJourney.id, searchQuery.startDate);
                    ViewBag.startLocation = searchQuery.startLocation;
                    ViewBag.endLocation = searchQuery.endLocation;
                    ViewBag.startDate = ""
                        + searchQuery.startDate.Day
                        + "/"
                        + searchQuery.startDate.Month
                        + "/"
                        + searchQuery.startDate.Year
                        + "";
                    ViewBag.noResult = false;
                }
                catch
                {
                    ViewBag.noResult = true;
                }
            }
            else
            {
                ViewBag.noResult = true;
            }
            // Search box
            var stations = new List<string>();
            var busTypes = new List<string>();
            try
            {
                stations = (from s in db.Stations select s.name).ToList();
                busTypes = (from b in db.Buses select b.type).Distinct().ToList();
            }
            catch { }
            ViewBag.stations = stations;
            ViewBag.busTypes = busTypes;
            return View();
        }

        public ActionResult Checkout()
        {
            return View();
        }


        [HttpPost]
        public ActionResult Checkout(FormCollection formData)
        {
            List<Ticket> tickets = ClientLogicModels.SaveBookingInformation(formData);
            var bookingDetailID = tickets.FirstOrDefault().BookingDetail.id;
            if (formData["paymentOption"] == "paypal")
            {
                APIContext apiContext = PaypalConfiguration.GetAPIContext();
                var baseURI = Request.Url.Scheme + "://" + Request.Url.Authority + "/Home/PaymentWithPayPal/" + bookingDetailID + "/?";
                var guid = Convert.ToString((new Random()).Next(100000));
                var createdPayment = PaypalPaymentAction.CreatePayment(apiContext, baseURI + "guid=" + guid, tickets);
                var links = createdPayment.links.GetEnumerator();
                var paypalRedirectUrl = "";
                while (links.MoveNext())
                {
                    Links lnk = links.Current;
                    if (lnk.rel.ToLower().Trim().Equals("approval_url"))
                    {
                        paypalRedirectUrl = lnk.href;
                    }
                }
                Session.Add(guid, createdPayment.id);
                return Redirect(paypalRedirectUrl);
            }
            return RedirectToAction("CheckoutSuccess", new { id = bookingDetailID });
        }

        public ActionResult PaymentWithPayPal(int id, string Cancel = null)
        {
            try
            {
                APIContext apiContext = PaypalConfiguration.GetAPIContext();
                var guid = Request.Params["guid"];
                string payerId = Request.Params["PayerID"];
                var executedPayment = PaypalPaymentAction.ExecutePayment(apiContext, payerId, Session[guid] as string);
                if (executedPayment.state.ToLower() != "approved")
                {
                    return RedirectToAction("CheckoutFail");
                }
            }
            catch
            {
                return RedirectToAction("CheckoutFail");
            }
            return RedirectToAction("CheckoutSuccess", new { id = id });
        }

        public ActionResult CheckoutSuccess(int id)
        {
            var bookingDetail = db.BookingDetails.Find(id);
            if (bookingDetail != null)
            {
                ViewBag.bookingDetail = bookingDetail;
                return View();
            }
            return RedirectToAction("CheckoutFail");
        }

        public ActionResult CheckoutFail()
        {
            return View();
        }
    }
}