﻿using G2OnlineBusTicket.Models;
using G2OnlineBusTicket.Models.Api;
using G2OnlineBusTicket.Models.Api.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace G2OnlineBusTicket.Controllers
{
    public class OBTApiController : ApiController
    {
        public G2OnlineBusTicketEntities db = ClientLogicModels.db;
        // DONE
        public ICollection<StationResult> GetStations() {
            var result = new List<StationResult>();
            var stations = (from s in db.Stations select s).ToList();
            foreach(var s in stations)
            {
                if (s.name != null && s.lat != null && s.lng != null) {
                    result.Add(new StationResult
                    {
                        id = s.id,
                        description = s.description,
                        lat = s.lat,
                        lng = s.lng
                    });
                }
            }
            return result;
        }

        public bool GetSeatStatus(int id, int journeyID, DateTime startDate)
        {
            var result = ClientLogicModels.IsSeatBooked(id, journeyID, startDate);
            if(!result)
            {
                db.Seats.Find(id).expired_at = DateTime.Now.AddMinutes(5);
                db.SaveChanges();
            }
            return result;
        }

        public bool GetCancelSatus(int id)
        {
            db.Seats.Find(id).expired_at = DateTime.Now;
            db.SaveChanges();
            return true;
        }

        public int GetDistance(int id, int journeyID, string startLocation, string endLocation)
        {
            var result = 0;
            var isDone = false;
            var incremental = false;
            var journey = db.Journeys.Find(journeyID);
            if (journey == null) { return result; }
            foreach(var route in journey.Routes)
            { 
                if (!isDone && incremental)
                {
                    result += route.distance_from_previous_station;
                }

                if (!isDone && route.Station.name == startLocation)
                {
                    incremental = true;
                }

                if (!isDone && route.Station.name == endLocation)
                {
                    incremental = false;
                    isDone = true;
                }
            }
            return result;
        }

        public decimal GetPrice(int id, string busType, int distance)
        {
            var result = 0;
            switch(busType)
            {
                case "Express":
                    result = 10000 * distance;
                    break;
                case "Volo":
                    result = 5000 * distance;
                    break;
            }
            return result;
        }
    }
}
